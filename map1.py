
import folium

import pandas
data = pandas.read_csv("Volcanoes.txt")
lat=list(data["LAT"])
lon=list(data["LON"])
elev = list (data["ELEV"])

#create a function to categorise volcanoes by height
def colour_producer(elevation):
    if elevation <1000:
       return "green"
    elif   1000<= elevation <2999:
       return "orange"
    else:
        return "red"
# setting map initial view & zoom
map = folium.Map(location=[55.119864,-1.547544],zoom_start=4, tiles="Mapbox Bright")

# adding markers
fg = folium.FeatureGroup(name="Points of Interest")
for coordinates in [[55.119864, -1.547544],[52.0,0.0]]:
    fg.add_child(folium.Marker(location=coordinates, popup="Hi there!",icon=folium.Icon(color='green')))

#adding markers from a file
fgv = folium.FeatureGroup(name="Volcanoes")
for lt, ln, el in zip(lat,lon, elev):
    #print(type(el))
    fgv.add_child(folium.CircleMarker(location=[lt,ln], radius = 6, popup=str(el)+" m", fill_color=colour_producer(el), color="grey", fill_opacity=0.7))

#adding a third layer showing population by country
fgp = folium.FeatureGroup(name="Population")
fgp.add_child(folium.GeoJson(data=open("world.json", "r", encoding="utf-8-sig").read(),
style_function=lambda x:{"fillColor":"green" if x["properties"] ["POP2005"] <10000000
else "orange" if 10000000 <=x["properties"] ["POP2005"] <70000000
else "red"}))

#adding a fourth layer showing population by area
fga = folium.FeatureGroup(name="Area")
fga.add_child(folium.GeoJson(data=open("world.json", "r", encoding="utf-8-sig").read(),
style_function=lambda x:{"fillColor":"gold" if x["properties"] ["AREA"] <24193
else "blue" if  x["properties"] ["AREA"] ==24193
else "red"}))

map.add_child(fg)
map.add_child(fgv)
map.add_child(fgp)
map.add_child(fga)

map.add_child(folium.LayerControl())
map.save("Map1.html")
